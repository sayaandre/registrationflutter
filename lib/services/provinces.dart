import 'package:http/http.dart';
import 'dart:convert';

class GetProvinces{
  bool error;
  String message;
  List<Provinces> semuaprovinsi;

  GetProvinces(this.error, this.message, [this.semuaprovinsi]);

  factory GetProvinces.fromJson(dynamic json) {
    if (json['semuaprovinsi'] != null) {
      var tagObjsJson = json['semuaprovinsi'] as List;
      List<Provinces> _tags = tagObjsJson.map((tagJson) => Provinces.fromJson(tagJson)).toList();

      return GetProvinces(
          json['error'] as bool,
          json['message'] as String,
          _tags
      );
    }
    else {
      return GetProvinces(
          json['error'] as bool,
          json['message'] as String,
      );
    }
  }
}

class Provinces{
  String id;
  String name;

  Provinces({this.id, this.name});

  Provinces.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['nama'];

  Map<String, dynamic> toJson() =>
      {
        'id' : id,
        'nama': name
      };
}