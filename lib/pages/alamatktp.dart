import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:registration/services/provinces.dart';
import 'package:registration/pages/review.dart';
import 'dart:convert';

class AlamatKTP extends StatefulWidget {

  final String nik, name, accnum, lastedu, dob;

  AlamatKTP({Key key, @required this.nik, @required this.name, @required this.accnum,
    @required this.lastedu, @required this.dob}) : super(key: key);

  @override
  _AlamatKTPState createState() => _AlamatKTPState();

}

class _AlamatKTPState extends State<AlamatKTP> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> _colors = <String>['', 'Rumah', 'Kantor'];
  String residence = '';
  List<dynamic> tests;
  List<DropdownMenuItem<Provinces>> _dropdownMenuItems;
  Provinces _selectedCompany;

  //================================================================================
  String _mySelection;
  final String url = "http://dev.farizdotid.com/api/daerahindonesia/provinsi";
  List data = List(); //edited line
  Future<String> getSWData() async {
    var res = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);
    setState(() {
      data = resBody['semuaprovinsi'];
    });
    return "Sucess";
  }
  //================================================================================

  List<DropdownMenuItem<Provinces>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<Provinces>> items = List();
    for (Provinces company in companies) {
      items.add(
        DropdownMenuItem(
          value: company,
          child: Text(company.name),
        ),
      );
    }
    return items;
  }

  @override
  void initState() {
    super.initState();
    this.getSWData();
  }

  onChangeDropdownItem(Provinces selectedCompany) {
    setState(() {
      _selectedCompany = selectedCompany;
    });
  }

  final addresscontroller = TextEditingController();
  final blokcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Alamat KTP'),
        centerTitle: true,
        backgroundColor: Colors.grey[850],
        elevation: 0.0,
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.map),
                      hintText: 'Enter Address',
                      labelText: 'Address',
                    ),
                    maxLines: 5,
                    keyboardType: TextInputType.text,
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(100),
                    ],
                    controller: addresscontroller,
                  ),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.home),
                          labelText: 'Residence',
                        ),
                        isEmpty: residence == '',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: residence,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                residence = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _colors.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 20.0,),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.streetview),
                      hintText: 'Enter No Blok',
                      labelText: 'No Blok',
                    ),
                    keyboardType: TextInputType.text,
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(10),
                    ],
                    controller: blokcontroller,
                  ),
                  SizedBox(height: 20.0,),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.explore),
                          labelText: 'Province',
                        ),
                        isEmpty: true,
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            items: data.map((item) {
                              return new DropdownMenuItem(
                                child: new Text(item['nama']),
                                value: item['id'].toString(),
                              );
                            }).toList(),
                            onChanged: (newVal) {
                              setState(() {
                                _mySelection = newVal;
                              });
                            },
                            value: _mySelection,
                          ),
//                          child: new DropdownButton(
//                            value: _selectedCompany,
//                            items: _dropdownMenuItems,
//                            isDense: true,
//                            onChanged: onChangeDropdownItem,
//                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 20.0,),
                  new Container(
                      padding: const EdgeInsets.only(left: 0.0, top: 20.0),
                      child: new RaisedButton(
                        child: const Text('Submit'),
                        onPressed: cekAlamatKTP,
                      )),
                ],
              ))),
    );
  }

  void cekAlamatKTP() {
    String address = addresscontroller.text;
    String blok = blokcontroller.text;
    print('$_mySelection and $residence');
    if(address.length > 0 && residence.length > 0 && blok.length > 0 && _mySelection.length > 0)
    {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Review(nik: widget.nik, name: widget.name, accnum: widget.accnum,
        lastedu: widget.lastedu, dob: widget.dob, address: address, residence: residence, blok: blok, province: _mySelection,)));
    }
    else
    {

    }
  }
}
