import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:registration/pages/alamatktp.dart';

class DataDiri extends StatefulWidget {
  @override
  _DataDiriState createState() => _DataDiriState();
}

class _DataDiriState extends State<DataDiri> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> _colors = <String>['', 'SD', 'SMP', 'SMA', 'S1', 'S2', 'S3'];
  String edu = '';

  DateTime selectedDate = DateTime.now();
  String strSelectedDate = 'Date of Birth';

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101)
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        String formattedDate = DateFormat('dd-MM-yyyy').format(picked);
        strSelectedDate = formattedDate;
      });
  }

  final nikcontroller = TextEditingController();
  final namecontroller = TextEditingController();
  final accnumcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Data Diri'),
        centerTitle: true,
        backgroundColor: Colors.grey[850],
        elevation: 0.0,
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.person),
                      hintText: 'Enter NIK',
                      labelText: 'NIK',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(16),
                    ],
                    controller: nikcontroller,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.assignment_ind),
                      hintText: 'Enter your Name',
                      labelText: 'Name',
                    ),
                    keyboardType: TextInputType.text,
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(10),
                    ],
                    controller: namecontroller,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.account_balance_wallet),
                      hintText: 'Enter your Account Number',
                      labelText: 'Account Number',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      new LengthLimitingTextInputFormatter(8),
                    ],
                    controller: accnumcontroller,
                  ),
                  new FormField(
                    builder: (FormFieldState state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.school),
                          labelText: 'Last Education',
                        ),
                        isEmpty: edu == '',
                        child: new DropdownButtonHideUnderline(
                          child: new DropdownButton(
                            value: edu,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                edu = newValue;
                                state.didChange(newValue);
                              });
                            },
                            items: _colors.map((String value) {
                              return new DropdownMenuItem(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: 20.0,),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    elevation: 4.0,
                    onPressed: () {
                      _selectDate(context);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 50.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.date_range,
                                      size: 18.0,
                                    ),
                                    Text(
                                      " $strSelectedDate",
                                      style: TextStyle(
                                          fontSize: 18.0),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          Text(
                            "  Change",
                            style: TextStyle(
                                fontSize: 18.0),
                          ),
                        ],
                      ),
                    ),
                    color: Colors.white,
                  ),
                  new Container(
                      padding: const EdgeInsets.only(left: 0.0, top: 20.0),
                      child: new RaisedButton(
                        child: const Text('Submit'),
                        onPressed: cekDataDiri,
                      )),
                ],
              ))),
    );
  }

  void cekDataDiri() {
    String nik = nikcontroller.text;
    String name = namecontroller.text;
    String accnum = accnumcontroller.text;
    if(nik.length > 0 && name.length > 0 && accnum.length > 0 && edu.length > 0 && strSelectedDate != 'Date of Birth')
    {
      Navigator.push(context, MaterialPageRoute(builder: (context) => AlamatKTP(nik: nik, name: name, accnum: accnum,
        lastedu: edu, dob: strSelectedDate,)));
    }
    else
    {
      
    }
  }
}
