class Identitas{
  final String nik;
  final String name;
  final String accnum;
  final String lastedu;
  final String dob;
  final String address;
  final String residence;
  final String blok;
  final String province;

  Identitas(this.nik, this.name, this.accnum, this.lastedu, this.dob, this.address, this.residence, this.blok, this.province);
}